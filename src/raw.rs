use crate::consts::{gear, M_TABLE, N_TABLE};
use crate::Fingerprint;

#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub mod x86;

/// Performs GEAR based MinHash fingerprinting of the provided data, using a simple,
/// scalar fallback implementation
pub fn fingerprint_scalar(data: &[u8]) -> Fingerprint {
    let mut mins = [u32::MAX; 24];
    let mut hash = 0;
    for byte in data.iter().copied() {
        hash = gear(hash, byte);
        for i in 0..24 {
            mins[i] = u32::min(
                mins[i],
                hash.overflowing_mul(N_TABLE.0[i])
                    .0
                    .overflowing_add(M_TABLE.0[i])
                    .0,
            );
        }
    }
    Fingerprint { fingerprint: mins }
}
