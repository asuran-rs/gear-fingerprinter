use cfg_if::cfg_if;
use criterion::*;
use gear_fingerprinter::raw::fingerprint_scalar;
use gear_fingerprinter::*;
use rand::prelude::*;

fn get_test_data(size: usize) -> (Vec<u8>, Vec<u8>) {
    let mut vec = vec![0_u8; size];
    rand::thread_rng().fill_bytes(&mut vec);
    (vec![0_u8; size], vec)
}

fn bench_fingerprint(c: &mut Criterion, name: &str, function: impl Fn(&[u8]) -> Fingerprint) {
    let mut group = c.benchmark_group(name);
    group.sample_size(100);
    group.measurement_time(std::time::Duration::from_secs(10));

    for size in &[32] {
        group.throughput(Throughput::Bytes(*size as u64 * 1000));
        let (zeros, random) = get_test_data(*size * 1000);
        group.bench_function(format!("{}KB Zeros", size), |b| b.iter(|| function(&zeros)));
        group.bench_function(format!("{}KB Random", size), |b| {
            b.iter(|| function(&random))
        });
    }

    group.finish();
}

fn scalar(c: &mut Criterion) {
    bench_fingerprint(c, "scalar", fingerprint_scalar);
}

cfg_if! {
    if #[cfg(any(target_arch = "x86", target_arch = "x86_64"))] {
        use gear_fingerprinter::raw::x86::*;
        fn avx2(c: &mut Criterion) {
            fn tmp(data: &[u8]) -> Fingerprint {
                unsafe {fingerprint_avx2(data)}
            }
            bench_fingerprint(c, "avx2", tmp)
        }
        fn sse41(c: &mut Criterion) {
            fn tmp(data: &[u8]) -> Fingerprint {
                unsafe {fingerprint_sse41(data)}
            }
            bench_fingerprint(c, "sse41", tmp)
        }
        criterion_group!(benches, scalar, avx2, sse41);
    } else {
        criteron_group!(benches, scalar);
    }
}

criterion_main!(benches);
